
function drawer(price, cash, cid) {

	var BILLS = {
		"PENNY" : 0.01,
		"NICKEL" : 0.05,
		"DIME" : 0.1,
		"QUARTER" : 0.25,
		"ONE" : 1,
		"FIVE" : 5,
		"TEN" : 10,
		"TWENTY" : 20,
		"ONE HUNDRED" : 100
	};

	var change = (cash - price).toFixed(2);
	var startChange = (cash - price).toFixed(2);

	var changeDrawer = [];
	var sum=0;
	for (var i = cid.length-1; i >= 0; i--) {
		var drawer = cid[i];
		var billType = drawer[0];
		var billValue = BILLS[billType];
		var maxCashInBill = drawer[1];
		sum+=maxCashInBill;

		var numberOfBills = Math.floor( change / billValue );
		console.log("Number of given " + billValue + " = " + numberOfBills + ' ---- ' );
		if (numberOfBills > 0 && maxCashInBill > 0) {
			var changeValueForCurrentBillType = numberOfBills * billValue;
			changeValueForCurrentBillType = changeValueForCurrentBillType > maxCashInBill ? maxCashInBill : changeValueForCurrentBillType;		
//			change=change.toFixed(2);
			console.log("("+change+") Max = " + maxCashInBill + " change in " + billValue + " still got change to give out == " + change);

			change -= changeValueForCurrentBillType;
			change = +change.toFixed(2);


			changeDrawer.push([ billType, changeValueForCurrentBillType ]);
		} else {
			continue;
		}
if(Math.round(change)==0.01) i++;
	}

	//count cid money
	var cashInDrawer = 0;
console.log("Change = " + change.toFixed(2) + " Sum = " + sum+" startChange = " + startChange);
if ( startChange-sum === 0 ) return "Closed";
if (change > 0 )	return "Insufficient Funds";


	// Here is your change, ma'am.
	return changeDrawer;
}

// Example cash-in-drawer array:
// [["PENNY", 1.01],
// ["NICKEL", 2.05],
// ["DIME", 3.10],
// ["QUARTER", 4.25],
// ["ONE", 90.00],
// ["FIVE", 55.00],
// ["TEN", 20.00],
// ["TWENTY", 60.00],
// ["ONE HUNDRED", 100.00]]

var a = drawer(19.50, 20.00, [ [ "PENNY", 1.01 ], [ "NICKEL", 2.05 ],
		[ "DIME", 3.10 ], [ "QUARTER", 4.25 ], [ "ONE", 90.00 ],
		[ "FIVE", 55.00 ], [ "TEN", 20.00 ], [ "TWENTY", 60.00 ],
		[ "ONE HUNDRED", 100.00 ] ]);
console.log(a);
a = drawer(19.50, 20.00, [["PENNY", 0.50], ["NICKEL", 0], ["DIME", 0], ["QUARTER", 0], ["ONE", 0], ["FIVE", 0], ["TEN", 0], ["TWENTY", 0], ["ONE HUNDRED", 0]]);
console.log(a);
a=drawer(3.26, 100.00, [["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.10], ["QUARTER", 4.25], ["ONE", 90.00], ["FIVE", 55.00], ["TEN", 20.00], ["TWENTY", 60.00], ["ONE HUNDRED", 100.00]]) ;
console.log(a);
a=drawer(19.50, 20.00, [["PENNY", 0.50], ["NICKEL", 0], ["DIME", 0], ["QUARTER", 0], ["ONE", 0], ["FIVE", 0], ["TEN", 0], ["TWENTY", 0], ["ONE HUNDRED", 0]])
console.log(a);




