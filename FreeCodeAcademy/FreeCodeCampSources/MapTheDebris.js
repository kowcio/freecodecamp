function orbitalPeriod(arr) {
  var GM = 398600.4418;
  var earthRadius = 6367.4447;

    var a = 2 * Math.PI;
    var newArr = [];
    //function for object we get 
    var getOrbPeriod = function (obj) {
        var c = Math.pow(earthRadius + obj.avgAlt ,3);
        var b = Math.sqrt(c/GM);
        var orbPeriod = Math.round(a * b);
        //we delete the property of this object
        delete obj.avgAlt;
        //add the property to the object with the period
        obj.orbitalPeriod = orbPeriod;
        return obj;
    };

    for (var elem in arr){
      console.log(arr[elem]);
        var orbPeriod=getOrbPeriod(arr[elem]);
        console.log(orbPeriod);
        //add to the new array with results objects
		newArr.push(orbPeriod);
    }
    return newArr;
}

orbitalPeriod([{name : "sputnik", avgAlt : 35873.5553}]);
