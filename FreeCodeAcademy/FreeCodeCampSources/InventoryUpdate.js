function inventory(arr1, arr2) {
    // All inventory must be accounted for or you're fired!
	var diff=arr1;
	if (arr1.length==0 ) return arr2.sort(compareSecondColumn);;
	if (arr2.length==0 ) return arr1.sort(compareSecondColumn);;
	
	for(var i in arr2){
		var item2=arr2[i];
		arr1 = update(arr1,item2);
	}
	console.log("New array whole = " + arr1.sort(compareSecondColumn));
	
    return arr1.sort(compareSecondColumn);
}

function update(arr, item){
	var update = false;
	for (var i in arr){
		
		var isEqual = arr[i][1] == item[1];
		console.log(i + " Check on " + arr[i][1] + " vs " + item[1] + " compare = " + isEqual + " ar.length = " + arr.length);

		if (isEqual) {
			console.log( i +" Update item = " + item);
			arr[i][0]+=item[0];
			console.log("After update = " + arr[i][0]);
			update=true;
		}
		//if didn`t found utem
		if(i==arr.length-1 && !update){
			//push new item to arr
			console.log(i + " Add new item = " + item);
			arr.push(item);
		}	
		
		console.log(arr);
	}

		
	return  arr;
}


function compareSecondColumn(a, b) {
	 return a[1] > b[1];
}


// Example inventory lists
var curInv = [
    [21, 'Bowling Ball'],
    [2, 'Dirty Sock'],
    [1, 'Hair Pin'],
    [5, 'Microphone']
];

var newInv = [
    [2, 'Hair Pin'],
    [3, 'Half-Eaten Apple'],
    [67, 'Bowling Ball'],
    [7, 'Toothpaste']
];

inventory(curInv, newInv);
