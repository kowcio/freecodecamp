function uniteUnique(arr1, arr2, arr3) {
  // Creates an empty array to store our final result.
  var finalArray = [];

  // iterate over all argument arrays
  for (var i = 0; i < arguments.length; i++) {
    var argArr = arguments[i];

    // check values in array from arguments
    for (var j = 0; j < argArr.length; j++) {
      var valueInArgArr = argArr[j];

      // if < 0 we didn`t found the value in final array
      if (finalArray.indexOf(valueInArgArr) < 0) {
        finalArray.push(valueInArgArr);
      }else{
    	  console.log("We found the value, skip and move forward.");
      }
      
    }
  }

  return finalArray;
}

var a = uniteUnique([1, 3, 2], [5, 2, 1, 4], [2, 1]);
console.log(a);
var b = uniteUnique([1, 3, 2], [1, [5]], [2, [4]]);
console.log(b);
var c = uniteUnique([1, 2, 3], [5, 2, 1, 4], [2, 1], [6, 7, 8]);
console.log(c);
