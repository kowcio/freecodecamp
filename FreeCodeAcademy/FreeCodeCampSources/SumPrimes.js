function sumPrimes(num) {
	
	var numPrimes = 0;
	var sumPrimes = 0;
	var i = 2;
console.log("Max = " + num);
	while (numPrimes < num) {
	    var isprime=isPrime(i);
		if (isprime) {
	        // Add this number to the sum of primes
	    	console.log("Prime = " + i + " / " + numPrimes + " isPrime= " + isprime + " num = " + num);
	        sumPrimes += i;
	        numPrimes++;
	    }
	    i++;
	}
	console.log("sum = " + sumPrimes);
	return sumPrimes;
}

function isPrime(num) {
    // Loop through all canidates of numbers that might divide
    for (var x = 2; x <= Math.round(num/2); x++)
        // Check if it divides cleanly
        if (num % x === 0)
            return false
    // Otherwise, it is a prime
    return true
}

sumPrimes(10);
