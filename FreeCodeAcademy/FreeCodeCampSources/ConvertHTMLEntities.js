function convert(str) {
  
	
	str = htmlEscape(str);
	console.log(str);
	return str;
}

function htmlEscape(str) {
    return str
    .replace(/&/g, '&amp;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#39;')        
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;');
}

convert('Dolce & Gabbana');
