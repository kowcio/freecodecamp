function ordinal(i) {
	var j = i % 10, k = i % 100;
	if (j == 1 && k != 11) {
		return i + "st";
	}
	if (j == 2 && k != 12) {
		return i + "nd";
	}
	if (j == 3 && k != 13) {
		return i + "rd";
	}
	return i + "th";
}

function getDate(date) {
	return new Date(Date.parse(date.replace(/-/g, '/')));
}

function monthDiff(date1, date2) {
	var month2 = date2.getUTCFullYear() * 12 + date2.getUTCMonth();
	var month1 = date1.getUTCFullYear() * 12 + date1.getUTCMonth();

	var diff = month2 - month1;

	if ((date1.getDate() - date2.getDate()) <= 0) {
		diff += 1;
		// console.log("13th month");
	}
	// console.log(diff+'diff');
	return diff;
}

function friendly(str) {
	var monthNames = [ "January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "December" ];

	var dateFrom = getDate(str[0]);
	var dateTo = getDate(str[1]);
	console.log(dateFrom.getUTCFullYear());
	console.log(dateTo.getUTCFullYear());
	console.log(dateFrom.getUTCFullYear() === (dateTo.getUTCFullYear() - 1));
	console.log(monthDiff(dateFrom, dateTo));
	result = [];
	// same day

	if (dateFrom.getTime() - dateTo.getTime() === 0) {
		result.push(monthNames[dateFrom.getMonth()] + ' '
				+ ordinal(dateFrom.getDate()) + ', '
				+ dateFrom.getUTCFullYear());
		console.log(0);
	}
	// in the same month that it begins
	else if (dateFrom.getMonth() === dateTo.getMonth()
			&& monthDiff(dateFrom, dateTo) < 12) {
		result.push(monthNames[dateFrom.getMonth()] + " "
				+ ordinal(dateFrom.getDate()));
		result.push(ordinal(dateTo.getDate()));
		console.log(1);
	}
	// ending in the same year
	else if (dateFrom.getUTCFullYear() === dateTo.getUTCFullYear()
	// ||
	// (
	// dateFrom.getUTCFullYear()===(dateTo.getUTCFullYear()-1)
	// && dateFrom.getMonth==dateTo.getMonth()
	//
	// // && monthDiff(dateFrom, dateTo)<=12
	// )
	) {
		result.push(monthNames[dateFrom.getMonth()] + " "
				+ ordinal(dateFrom.getDate()) + ", " + dateTo.getUTCFullYear());
		result.push(monthNames[dateTo.getMonth()] + " "
				+ ordinal(dateTo.getDate()));
		console.log(2);
	}
	// less than a year from when it begins, do not display the ending yea
	else if (dateFrom.getMonth() === dateTo.getMonth()
			&& monthDiff(dateFrom, dateTo) < 12) {
		result.push(monthNames[dateFrom.getMonth()] + " "
				+ ordinal(dateFrom.getDate()) + ", "
				+ dateFrom.getUTCFullYear());
		result.push(monthNames[dateTo.getMonth()] + " "
				+ ordinal(dateTo.getDate()));
		console.log(2.5);

	} else if (monthDiff(dateFrom, dateTo) < 12) {
		result.push(monthNames[dateFrom.getMonth()] + " "
				+ ordinal(dateFrom.getDate()));
		result.push(monthNames[dateTo.getMonth()] + " "
				+ ordinal(dateTo.getDate()));
		console.log(3);
	}
	// begins in the current year and ends within one year, the year should not
	// be
	// displayed at the beginning
	else if (monthDiff(dateFrom, dateTo) >= 12) {
		result.push(monthNames[dateFrom.getMonth()] + " "
				+ ordinal(dateFrom.getDate()) + ", " + dateFrom.getFullYear());
		result.push(monthNames[dateTo.getMonth()] + " "
				+ ordinal(dateTo.getDate()) + ", " + dateTo.getFullYear());
		console.log(4);
	}
	console.log(result);

	return result;
}

var a = friendly([ "2022-09-05", "2023-09-04" ])
		+ ' -------------- ["September 5th, 2022","September 4th"].';
console.log('a=' + a);
// var a =friendly(["2016-07-01", "2016-07-04"]) + '---------- " + ["July
// 1st","4th"] ';
// console.log('a='+a);
// a =friendly(["2016-12-01", "2017-02-03"]) + '-----------"December
// 1st","February 3rd"]. ';
// console.log('a='+a);
// a =friendly(["2016-12-01", "2018-02-03"]) +'-----------["December 1st,
// 2016","February 3rd, 2018"]';
// console.log('a='+a);
// a =friendly(["2017-03-01", "2017-05-05"])+'-------------"March 1st,
// 2017","May 5th"';
// console.log('a='+a);
// a =friendly(["2018-01-13", "2018-01-13"])+'-------------["January 13th,
// 2018"]';
// console.log('a='+a);
a = friendly([ "2022-09-05", "2023-09-05" ])
		+ '--------- ["September 5th, 2022","September 5th, 2023"]';
console.log(a);
