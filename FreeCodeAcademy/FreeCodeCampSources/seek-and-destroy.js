function destroyer(arr) {
   var args   = [].slice.call( arguments, 0 );
  var array  = args.shift();
  var result = [];

  array
    .forEach( function( item ) {
      if ( args.indexOf( item ) < 0 ) {
        result.push( item );
      }
      
    });

  return result;
}

var q = destroyer([1, 2, 3, 1, 2, 3], 2, 3);
console.log(q);