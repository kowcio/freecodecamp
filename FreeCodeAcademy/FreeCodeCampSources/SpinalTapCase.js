//function spinalCase(str) {
//	// "It's such a fine line between stupid, and clever."
//	// --David St. Hubbins
//
//	var temp = "";
//	if (str.charAt(0) === str.charAt(0).toUpperCase()) {
//		temp = str.charAt(0).toLowerCase();
//		str = str.substr(1, str.length);
//		str = str.replace(/([A-Z]{1})/g, "-$1");
//	} else {
//		str = str.replace(/([A-Z]{1})/g, "-$1");
//	}
//
//	str = str.replace(/[_]{1}/g, "");
//	str = str.replace(/[ ]{1}/g, "-");
//	str = temp + str;
//	console.log(str);
//	return str;
//}

spinalCase('This Is Spinal Tap');
spinalCase('The_Andy_Griffith_Show');
spinalCase('thisIsSpinalTap');
spinalCase('Teletubbies say Eh-oh');

function spinalCase(str) {

	  var result = str
	    .replace( /[ -?_]/g, '-' )//replace specials to -
	    .replace( /[A-Z]/g, '-$&' )//replace with upper case and -
	    .replace( /--/g, '-' )//replace 2 with 1
	    .replace( /^-/, '' )//make start - into nothing
	    .toLowerCase();

	 console.log(result);
	return result;
}
