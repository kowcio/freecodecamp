function reverse(s) {
	return s.toLowerCase().split("").reverse().join("");
}

function mod(n) {
        var modBool =  n%2;
        console.log("BOL="+modBool);
		return modBool;
}
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
function palindrome(str) {
	str = str.replaceAll(" ","").replaceAll("[,.]","").toLowerCase();
	console.log("String =" + str + " Length " + str.length);
	
	var length = str.length;
	var evenLength = mod(length);
	var lengthHalf = evenLength==1 ? length/2-0.5 : length/2;
	
	var half1 = str.substring(0, lengthHalf);
	
	if(evenLength==true){
		lengthHalf+=1;
	}
	var half2 = str.substring( lengthHalf, length);
	
	console.log("1st = " + half1 + "\n2nd = " + half2+ "  Half length = "+ lengthHalf + "  EvenLength="+evenLength);
	return (half1 == reverse(half2));
}

console.log("RESULT "+ palindrome("A man, a plan, a canal. Panama"));